import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { TextField, Button, LinearProgress } from '@material-ui/core';
import * as yup from "yup";
import { useFormik } from 'formik';

const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiTextField-root': {
      'margin-bottom': theme.spacing(1),
    },
  },
}));

const validationSchema = yup
  .object()
  .shape({
    price: yup
      .number()
      .typeError('price must be a number')
      .positive()
      .required()
      .test('length', 'price must be greater than 1000', val => {
        return val >= 1000 
      }),
    description: yup
      .string()
      .required(),
  });

const CarForm = ({ onSubmit, isLoading }) => {
  const classes = useStyles();
  const { handleSubmit, errors, touched, setFieldValue, setFieldTouched, resetForm } = useFormik({
    initialValues: {
      price: '',
      description: '',
    },
    validationSchema,
    onSubmit,
  })

  return (
    <form noValidate autoComplete="off" onSubmit={handleSubmit} className={classes.root}>
      <TextField
        id="price-field"
        name="price"
        label="Price"
        type="text"
        variant="outlined"
        required
        error={touched.price && Boolean(errors.price)}
        helperText={touched.price && errors.price || null}
        onChange={({ target }) => setFieldValue('price', target.value)}
        onBlur={() => setFieldTouched('price', true)}
        disabled={isLoading}
      />
      <TextField
        id="description-field"
        name="description"
        label="Description"
        rows={4}
        variant="outlined"
        fullWidth
        multiline
        required
        error={touched.description && Boolean(errors.description)}
        helperText={touched.description && errors.description || null}
        onChange={({ target }) => setFieldValue('description', target.value)}
        onBlur={() => setFieldTouched('description', true)}
        disabled={isLoading}
      />
      <Button
        type="submit"
        variant="outlined"
        disabled={isLoading}
      >
        Publish
      </Button>
      {isLoading && <LinearProgress />}
    </form>
  );
};

export default CarForm;
