import React from 'react';
import { makeStyles, Modal, Backdrop, Fade } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
  container: {
    maxHeight: '500px',
    overflow: 'scroll',
  }
}));

const ScreenshotModal = ({ screenshot, onClose }) => {
  const classes = useStyles();

  return (
    <Modal
      aria-labelledby="transition-modal-title"
      className={classes.modal}
      open={Boolean(screenshot)}
      onClose={onClose}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500,
      }}
    >
      <Fade in={Boolean(screenshot)}>
        <div className={classes.paper}>
          <h2 id="transition-modal-title">Publication preview</h2>
          <div className={classes.container}>
            <img src={screenshot} />
          </div>
        </div>
      </Fade>
    </Modal>
  );
};

export default ScreenshotModal;
