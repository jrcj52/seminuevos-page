import React, { useState } from 'react';
import { Modal, Typography, Container, Box } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import { fitsh } from 'fitsh';
import { HOST_API } from '../config';
import CarForm from '../components/CarForm';
import ScreenshotModal from '../components/ScreenshotModal';

export default function Index() {
  const [isPublishing, setIsPublishing] = useState(false);
  const [screenshot, setScreenshot] = useState(null);
  const [hasError, setHasError] = useState(false);

  const submit = async (fields) => {
    const api = fitsh(HOST_API);
    setIsPublishing(true);
    setHasError(false);

    try {
      const imageBlob = await api('car').post('blob', {
        price: fields.price,
        description: fields.description,
      });
      const imageUrl = URL.createObjectURL(imageBlob);
      setScreenshot(imageUrl);
    } catch (error) {
      console.error(error);
      setHasError(true);
    }
    setIsPublishing(false);
  };

  return (
    <>
      <Container maxWidth="sm">
        <Box my={4}>
          <Typography variant="h4" component="h1">
            Publish car to Seminuevos
          </Typography>
          <Typography variant="caption" component="p" color="textSecondary" paragraph>
            fill the fields and click to publish
          </Typography>
          <CarForm onSubmit={submit} isLoading={isPublishing} />
          {hasError && <Alert severity="error">An error has occurred</Alert>}
        </Box>
      </Container>
      <ScreenshotModal screenshot={screenshot} onClose={() => setScreenshot(null)} />
    </>
  );
}
